# STAD — Simple Two-level Attributes Document

[[_TOC_]]

STAD is a text-based document format for sharing structured data,
similar to JSON or YAML, but intentionally limited to a strict and
shallow subset of those formats, to ease omnidirectional data
interchange among languages, in particular native production and
consumption by simpler shell-style language environments such as bash
that do not easily support deeply structured data.  It is intended to
be a basis for application-specific document model uses.

A STAD document is a single dictionary with “variable”-like key names,
whose values can only be strings, arrays of strings, and dictionaries
of strings.  No further recursive nesting structure is allowed.

A STAD document can use one of currently 3 mutually convertible syntax
variants: **stad-json**, **stad-yaml** or **stad-bash**, as described
below.

Additionally, for importing/interchange purposes, a less strict
variant named **stad-basheval** is conditionally supported, that
requires a bash interpreter for parsing.

Additional syntax variants could be defined in the future, for
example, based on XML or xhtml, and would be acceptable as STAD
assuming they are mutually convertible to and from the variants
already described here, without loss of data or structure.

Domain-specfic extensions to STAD that add further constraints to the
basic document model are encouraged.

## Document Structure and Data Types

In all syntax variants, these rules apply regarding data types and
string formats:

The outer document structure is a single **Dictionary** (set of
Key/Value pairs where keys must be unique).

- Empty documents *are* allowed (i.e. with no key/value pairs).

First level document **Keys** *must* be in the format
```^[a-zA-Z_][a-zA-Z_0-9]*$``` (1 or more ASCII “word” chars, not
starting with a digit).

First level document **Values** *must* be 1 of these 3 types:

- **String** — example: ```"hi mom"```

  - String Values *may be* empty.

- **Array** (also known as: list) of zero or more String Values —
    example: ```("hi" "mom")```

  - String Values in arrays *may be* empty.

- **Dictionary** (also known as: associative array, hash array,
    structure, object) of zero or more Key/Value pairs — example:
    ```([mom]="Amy" [dad]="Joe")```

  - Keys *must* be Strings but *may not* be the empty string.

  - Second level dictionary Values *must be* Strings.

  - String Values in dictionaries *may be* empty.

All references to **Strings** above are presumed to be in UTF-8 format
with NUL (ASCII 0) *not* permitted, unless a more restrictive
character set is noted, for example in the case of top-level keys.

**Arrays** are order-preserving.   Additionally:

- The document syntax variants do not include numerical indexes for
  arrays.  Therefore:

  - STAD is neutral on the question of array element numbering
    (zero-based or one-based indexing).

  - “Sparse” arrays *are not supported* (indexes are always presumed
    to be sequential).  Users desiring sparse-array semantics may use
    STAD dictionaries (with string-based numeric keys) instead.

**Dictionaries** (top-level and second-level) are *not* guaranteed to
preserve key order when ingested.  Additionally:

- Keys *must* be unique within the dictionary.

- A program processing a document *may* choose to ingest, internally
    iterate over, and emit dictionary keys:

  - in the order originally given, or

  - in any other order, such as sorted order, or

  - in random/hash order.

- A program whose purpose is to convert a document between syntax
    variants, *should* preserve key order if possible.

- If key order is not to be preserved, sorted order *should* be used
    when emitting.

- A program emitting dictionary keys in sorted order *should* sort
    using LC_COLLATE=C semantics.

## JSON Syntax variant: stad-json

In the JSON syntax variant, the STAD document *must* be parseable
under the published JSON standard, but also *must* observe all of the
other general STAD limitations noted in this document:

It *must* consist only of a single top-level dictionary (“object”)
with keys and values as per the STAD standard.

First and second level keys *must* conform to the STAD standard.

It *may not* use any JSON-supported data types not mentioned in the STAD
standard, including null, numeric, or boolean values.

Clients *may* assume that the document will be parsed by a fully
conformant JSON parser.

Example of a valid stad-json document (note: comments are not allowed
in JSON):

```json
{
  "my_string": "hello",
  "my_greeting": "hi mom",
  "empty_string": "",
  "my_letters": [
    "a",
    "b",
    "c"
  ],
  "my_numbers": [
    "1",
    "2",
    "3"
  ],
  "my_names": [
    "Bob Smith",
    "Alice Jones"
  ],
  "my_pets": [
    "Fido",
    "Fluffy"
  ],
  "my_iguanas": [],
  "data_ports": {
    "443": "open",
    "80": "closed",
    "8080": "open,listen"
  },
  "data_types": {
    "effective_date": "date",
    "item_count": "integer",
    "null": ""
  },
  "drive_names": {}
}

```

## YAML Syntax variant: stad-yaml

In the YAML syntax variant, the STAD document *must* be parseable
under the published YAML standard, but also *must* observe all of the
other general STAD limitations noted in this document.

It *must* consist only of a single top-level dictionary (“object”)
with keys and values as per the STAD standard.

First and second level keys *must* conform to the STAD standard.

It *may not* use any YAML-supported data types not mentioned in the
STAD standard, including null, numeric, timestamp, or boolean values.
Users of STAD must arrange to express such values as strings and
relying software should operate accordingly.

Note: in the YAML syntax standard, comments are allowed but ignored
when parsing.

Clients *may* assume that the document will be parsed by a fully
conformant YAML parser.

Only the first document in a multi-document YAML file will be
considered when parsing a YAML file as a stad-yaml document.

Example of a valid stad-yaml document:

```yaml
---

# YAML permits comments

# Strings
my_string: hello            # a comment may go here
my_greeting: hi mom
empty_string: ''

# Arrays of strings
my_letters:
- a
- b
- c
my_numbers:                 # these are strings: "1", etc.
- '1'
- '2'
- '3'
my_names:
- Bob Smith
- Alice Jones
my_pets:
- Fido
- Fluffy
my_iguanas: []              # empty array

# Dictionaries of string keys and string values.
data_ports:
  '443': open
  '80': closed
  '8080': open,listen
data_types:
  effective_date: date     # comments allowed after hash elements
  item_count: integer
  'null': ''
drive_names: {}            # empty dictionary
```

## Bash Syntax Variant: stad-bash

In the Bash syntax variant, the STAD document is a source-able
(eval-able) fragment of legal bash code with a very limited permitted
syntax.

The bash syntax variant is designed to be sourced in a top-level script
context (key-value declarations will be interpreted as global variable
assignments) or in a function context (key-value declarations will be
interpreted as local variable assignments).

Creators *must not* assume that a bash-formatted STAD document will be
parsed by bash.  But it *must be* parseable by bash.

The document *must* consist only of any mixture of zero or more comment
lines, empty lines, and key-value declarations:

- A comment line must begin with ```#``` optionally preceded by
  whitespace, and any other content on that line will be ignored when
  parsing.

- An empty line may contain zero or more spaces or tabs, and will be
  ignored when parsing.

- A key-value declaration must begin on a new line starting with the
  word ```declare``` (optionally preceded by whitespace), and must be
  in one of these formats:

  - **String** ```declare -- my_str="value"```

  - **Array** ```declare -a my_array=("val1" "val2" ... "valx")```

  - **Dictionary** ```declare -A my_dict=(["key1"]="valx"
    ["key2"]="valx"... ["keyx"]="valx")```

Per usual bash syntax rules, any amount of whitespace is tolerated and
ignored between tokens (outside double-quoted strings) and after the
last token on a line, except: there *must not* be any whitespace on
either side of = signs used to pair the keys/values (i.e. bash variable
assignments); and there *must not* be any whitespace after the ```[``` or
before the ```]``` brackets identifying keys in the second-level
dictionaries.

Also per usual bash syntax rules, a comment can also be introduced at
the end of a ```declare``` statement by including a ```#``` character outside
of a double-quoted context.  Any further characters on that line will be
ignored.

In the above declaration format examples, strings shown in double-quotes
(```""```) must follow bash string quoting rules, but are further limited to
this subset of bash rules to be considered valid STAD bash syntax:

- strings that contain only 1 or more “word characters plus dash”
  ```(^[A-Za-z0-9_-]+$)``` *may* omit the surrounding ```""``` marks.

  - parsers *must* tolerate the presence or absence of the ```""```
    marks in this case but *should* emit with marks omitted when
    possible.

- all other strings (that are empty or that contain any
  non-word-or-dash characters), *must* be surrounded by ```""```
  marks.  (This intentionally differs from bash usage in which an
  empty right side of an ```=``` sign can signify an empty string
  value.)

- inside the ```""``` marks, the characters ``` " ``` (double-quote),
  ``` \ ``` (backslash), ``` ` ``` (backtick), or ``` $ ``` (dollar
  sign) appearing in a String *must* be escaped by a preceding
  backslash.  Per bash double-quoting rules, a backslash in any other
  context within the string is treated literally (i.e. included in the
  parsed string and not considered special).  In stad-bash, the
  ```!``` (exclamation point) is not special and is not escaped.

- ANSI or POSIX style string interpolation conveniences, such as
  ```\n``` for newline, ```\t``` for tab, ```\uXXXXX``` for Unicode,
  are not supported within double-quotes.  These and other control
  characters should be included as-is directly in the literal strings
  and are fully supported in stad-bash.  Therefore:

  - Double-quoted strings may include newlines (spanning lines
    in the document file) and include any other legal UTF-8
    characters.  NUL (ASCII 0) is not supported.

  - Strings may not hold arbitrary binary data.  Such data should be
    encoded in a UTF8 compatible encoding format and would require
    further decoding to binary by the receiving software.

- here-doc, single-quoting, ```$'...'``` (ANSI quotes) or other string
  literal syntaxes are legal bash, but are not supported in stad-bash.

All String values *must* be literals.  No interpolation syntaxes (such
as ```$XXX```, ```${XXX...}``` or ```$(...)``` are permitted).

No other variations of ```declare``` syntax may be used.  (For
example, ```declare -x```, ```declare -g```, ```declare -n```,
```export```, ```var=xyz```, etc. are not part of stad-bash.)

Outside of the allowed comments, blank lines, and key-value (variable)
declarations, any other bash-interpretable code or expressions *may
not* be used.  (The document contents must be context-neutral.)

Support of the above common bash idioms, context-aware interpolation
or any form of code execution can be had by using the stad-basheval
format, below.

Example of a valid stad-bash document:

```bash
# Comments and blank lines are allowed between items

# Strings
declare -- my_string=hello         # a comment may go here
declare -- my_greeting="hi mom"
declare -- empty_string=""

# Arrays of strings
declare -a my_letters=(a b c)
declare -a my_numbers=( 1 2 3 )    # these are strings: "1", etc.
declare -a my_names=( "Bob Smith" "Alice Jones" )
declare -a my_pets=(
  Fido   # comments allowed after array elements
  Fluffy
)
declare -a my_iguanas=()           # empty array

# Dictionaries of string keys and string values.
declare -A data_ports=([443]=open ["80"]="closed" ["8080"]="open,listen")
declare -A data_types=(
  [effective_date]=date            # comments allowed after hash elements
  [item_count]=integer
  [null]=""
)
declare -A drive_names=()          # empty dictionary
```

## Bash “eval” Syntax Variant: stad-basheval

It may be convenient to create a STAD-like bash script which includes
bash-like evaluations, calculations, interpolations, or quoting
constructs that are not part of the strict stad-bash syntax.

The *stad-basheval* tool is provided to support this use case.

It reads a stad-bash or arbitrary bash script concatenated from input
files or stdin, and rewrites declared variables as a single
well-formed stad-bash or stad-json document.  It intentionally only
gathers variables declared with ```declare --```, ```declare -a```, or
```declare -A``` encountered while executing the bash script; and only
emits those declarations that were successful and conformant to
stad-bash conventions.

The script is run in a sandbox (external invocation of bash) to avoid
polluting the caller's environment (in case the caller is running the
tool as a sourced library, which is supported).

Note: the code should not have side effects outside the script but we
can't enforce that.  *Thus, using stad-basheval carries the same risks
of executing any software and should never be used with potentially
tainted or untrustworthy content.  Best practice is to only use the
tool to interpret bash documents that your own software has generated,
free of potentially tainted user input.*

The provided script may not attempt to modify BASH read-only
variables.  The only environment variables it may read are
```$HOME```, ```$PATH```, and ```$USER```, and it may not modify
those.  Variables whose names begin with \_\_\_ (3 underscores) are
also disallowed.

The script can ```errexit``` (```set -e```) and other bash execution
flags if it wishes the stad-basheval code to stop on first error;
otherwise code will run and variable declarations after the failure
point will continue to be captured.

Generally the script itself *should not* produce output on stdout but
if it does, stad-basheval will redirect the output to stderr.  If the
script produces output on stderr, that will also be passed through
(and caller may capture it or not).  A stad-basheval script that
produces any stderr or stdout output when executed is considered
non-conforming and should be avoided.

In summary, the best stad-basheval document is one that consists only
of variable declarations in stad-bash style, but may interpolate the
$HOME variable and may use non-stad-bash quoting techniques and may
contain error control settings.

Example of a typical stad-basheval document:

```bash
set -e -u -o pipefail  # enable strict error checking

declare -- foo_path=$HOME/foo
declare -- delims=$'\t\n'
declare -- my_document=<<EOF
Long doc here
EOF

declare private_val=3  # will be ignored by stad-basheval

declare -a my_array=([0]=hi [1]=mom)
declare -A my_dict=([0]=hi [1]=mom)
declare -a my_answers=( $private_val 42 )
```

## Observations common to all Syntax Variants

A document accidentally containing non-unique keys might be legally
parsed by a STAD parser, but its behavior is not guaranteed in the
STAD standard.  A STAD parser *may* reject (treat as a warning or
error) or silently accept such a document.  If accepting, it *should*
use a “last-wins” approach to converting non-unique keys to unique
ones.

In the JSON syntax standard, there is no provision for comments.  Thus
there is no provision for preserving comments when parsing a STAD
document or converting between any STAD syntax variants.  Comment
information, though helpful for human consumers, is not considered to
be part of the STAD document content.

As noted, data types other than strings, arrays, and dictionaries are
not supported at all.  This means no native support for integers,
floats, booleans, nulls, or binary (non-UTF8) data.

There is also no support in STAD for nesting Arrays or Dictionaries as
values within other arrays or dictionaries, with the exception of the
first-level document dictionary.

Thus clients of data expressed in STAD format must arrange to express
such values as strings and relying software should operate accordingly.

Specialized extensions of STAD using user-defined conventions to support
extended data types *are actively encouraged*.

For example, a STAD-based DSL could declare that all, or certain,
string values which can be parsed as integers or floats or boolean
*should* or *must* be converted to those data types.  Or it could
define a syntax for declaring the extended data types of values via a
naming convention of the keys or by the inclusion of additional keys
containing metadata information; or for embedding nested arrays and
dictionaries in String values in any convenient parseable format; or
for embedding binary values as strings using ASCII-friendly encodings
such as base64 or punyCode; or could place further restrictions on the
legal values of dictionary keys, etc.

In cases of DSLs that extend STAD, strictly STAD-aware tools must still
be able to read and interchangeably convert the document using STAD
conventions and limitations; otherwise the ability to faithfully parse,
re-emit, or convert the document between syntax variants without
semantic loss, is not guaranteed.

Emitters of STAD documents are *strongly encouraged* to make the
documents human-friendly, for example, by including comments, or blank
lines, or pretty-printing JSON structures, etc.

## License

stad (c) 2022 by Chris Thorman

stad is licensed under a Creative Commons Attribution 4.0
International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
