#!/usr/bin/env bash
bats_require_minimum_version 1.5.0

load common.sh

TOOL=$STAD

guess_format_bash_empty() { #@test
  foo() { echo $'' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
guess_format_bash_declare() { #@test
  foo() { echo $'declare -- a=b' | "$TOOL"; }
  run foo
  assert_success
  assert_output 'declare -- a=b'
}
guess_format_yaml_empty() { #@test
  foo() { echo $'--- {}' | "$TOOL"; }
  run foo
  assert_success
  assert_output '--- {}'
}
guess_format_yaml_1() { #@test
  foo() { echo $'---\n{}' | "$TOOL"; }
  run foo
  assert_success
  assert_output '--- {}'
}
guess_format_yaml_2() { #@test
  foo() { echo $'---\na: b' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'---\na: b'
}
guess_format_json() { #@test
  foo() { echo $'{"a":"b"}' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'{\n  "a": "b"\n}'
}
format_basheval() { #@test
  foo() { echo $'declare -- a=b.c.d' | "$TOOL" -r basheval; }
  run foo
  assert_success
  assert_output $'declare -- a="b.c.d"'
}
format_basheval_as_bash_fails() { #@test
  foo() { echo $'declare -- a=b.c.d' | "$TOOL" -r bash; }
  run foo
  assert_failure
  assert_line -p $'stad-bash parsing failed'
}
format_output_bash() { #@test
  foo() { echo $'{"a":"b"}' | "$TOOL" -w bash; }
  run foo
  assert_success
  assert_output $'declare -- a=b'
}
format_output_yaml() { #@test
  foo() { echo $'{"a":"b"}' | "$TOOL" -w yaml; }
  run foo
  assert_success
  assert_output $'---\na: b'
}
format_output_ruby() { #@test
  foo() { echo $'{"a":"b"}' | "$TOOL" -w ruby; }
  run foo
  assert_success
  assert_output $'{"a"=>"b"}'
}
format_output_bash_array() { #@test
  foo() { echo $'{"a":["1","b"]}' | "$TOOL" -w bash; }
  run foo
  assert_success
  assert_output $'declare -a a=(\n  1\n  b\n)'
}
format_output_json_array() { #@test
  foo() { echo $'{"a":["1","b"]}' | "$TOOL" -w json; }
  run foo
  assert_success
  assert_output $'{\n  "a": [\n    "1",\n    "b"\n  ]\n}'
}
format_output_yaml_array() { #@test
  foo() { echo $'{"a":["1","b"]}' | "$TOOL" -w yaml; }
  run foo
  assert_success
  assert_output $'---\na:\n- '"'1'"$'\n- b'
}
format_output_ruby_array() { #@test
  foo() { echo $'{"a":["1","b"]}' | "$TOOL" -w ruby; }
  run foo
  assert_success
  assert_output $'{"a"=>["1", "b"]}'
}
format_output_bash_dict() { #@test
  foo() { echo $'{"a":{"1":"b"}}' | "$TOOL" -w bash; }
  run foo
  assert_success
  assert_output $'declare -A a=(\n  [1]=b\n)'
}
format_output_json_dict() { #@test
  foo() { echo $'{"a":{"1":"b"}}' | "$TOOL" -w json; }
  run foo
  assert_success
  assert_output $'{\n  "a": {\n    "1": "b"\n  }\n}'
}
format_output_yaml_dict() { #@test
  foo() { echo $'{"a":{"1":"b"}}' | "$TOOL" -w yaml; }
  run foo
  assert_success
  assert_output $'---\na:\n  '"'1'"$': b'
}
format_output_ruby_dict() { #@test
  foo() { echo $'{"a":{"1":"b"}}' | "$TOOL" -w ruby; }
  run foo
  assert_success
  assert_output $'{"a"=>{"1"=>"b"}}'
}
