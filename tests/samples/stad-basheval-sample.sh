# Can set error tolerance and tracing options if desired.
# set -euo pipefail
# set -x

# these will not be included since they lack "declare --" syntax
n=m
declare abc=def

# Note: the variable declaration order should be retained in output.
declare -- c=$'f`oo!b\tx"y\z$q'

# Subshell vars will be excluded...
(
  declare xy=202
  declare -- foo="$xy$xy"
)

# changes in a subshell also excluded, even if change by reference.
declare -- outer1=before
declare -- outer2=before
(
  outer1=after           # lost
  declare -n vref=outer2
  vref=after             # lost
)

# changes in braces will be effective.
declare -- outer3=before
declare -- outer4=before
{
  outer3=after            # retained
  declare -n vref=outer4
  vref=after              # retained
}
echo "cry wolf!!!" 1>&2

declare -a b=('x c' wookie)

# test of sorting of keys including multi-line keys.
declare -A a=([$'8\nb']='1 2' [$'8\na']='1 2' [3]=abc [f]=def)

# This should also fail if -e is set:
# false

# Most reserved bash vars are forbidden to be set in STAD; also PATH
# and HOME and USER are disallowed; and a few internal vars.

# declare -- _=0;
# declare -- UID=0;
# declare -a BASH_VERSINFO=0;

# declare -- PATH=foo
# declare -- HOME=x
# declare -- USER=x

# Vars starting with 3 underscores are also disallowed
# declare -- ___forbid=foo
# declare -- ___foobar=1

# this should be a no-op:
: ;

declare -- my_home="$HOME"

# echoing or any output to stdout is discouraged, but if done, should
# get redirected (once) to stderr:
echo "hi mom: $c"

declare -- empty_string=
declare -a empty_array=()
declare -A empty_dict=()

# This point should be reached if -e is not set:
declare -- z="reached the end!";

# comment at end
