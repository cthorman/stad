declare -- c="f\`oo!b	x\"y\\z\$q"
declare -- outer1=before
declare -- outer2=before
declare -- outer3=after
declare -- outer4=after
declare -a b=(
  "x c"
  wookie
  )
declare -A a=(
  [3]=abc
  ["8
a"]="1 2"
  ["8
b"]="1 2"
  [f]=def
  )
declare -- my_home="/Users/cthorman"
declare -- empty_string=""
declare -a empty_array=()
declare -A empty_dict=()
declare -- z="reached the end!"
