#!/usr/bin/env bash
bats_require_minimum_version 1.5.0

load common.sh

TOOL=$STAD_BASHEVAL

error_flag_sensitivity_errexit_no_is_default() { #@test
  foo() { echo $'false' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
error_flag_sensitivity_errexit_no() { #@test
  foo() { echo $'set +e; false' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
error_flag_sensitivity_errexit_yes() { #@test
  foo() { echo $'set -e; false' | "$TOOL"; }
  run foo
  assert_failure
}
empty_doc_1() { #@test
  foo() { echo '' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
empty_doc_2() { #@test
  foo() { echo $'\t\n\n' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
empty_doc_3() { #@test
  foo() { echo $'( : )' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
empty_doc_4() { #@test
  foo() { echo $'{ : ; }' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
declare_ignored_inside_function() { #@test
  foo() { echo $'x() { declare -- in=in; }; x; declare -- out=out' | "$TOOL"; }
  run foo
  assert_success
  assert_output 'declare -- out=out'
}
declare_ignored_inside_subshell() { #@test
  foo() { echo $'(declare -- in=in); declare -- out=out' | "$TOOL"; }
  run foo
  assert_success
  assert_output 'declare -- out=out'
}
declare_ignored_changes_inside_subshell() { #@test
  foo() { echo $'declare -- outer1=bef; declare -- outer2=bef; (outer1=aft; declare -n vref=outer2; vref=aft)' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -- outer1=bef\ndeclare -- outer2=bef'
}
declare_ignored_no_dashes() { #@test
  foo() { echo $'declare a=b' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
declare_ignored_no_declare() { #@test
  foo() { echo $'a=b' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
declare_ignored_export() { #@test
  foo() { echo $'export a=b' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
declare_ignored_dash_g() { #@test
  foo() { echo $'declare -g USER=b' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
declare_ignored_dash_x() { #@test
  foo() { echo $'declare -x a=b' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
input_from_stdin() { #@test
  foo() { echo $'declare -- a=b' | "$TOOL"; }
  run foo
  assert_success
  assert_output 'declare -- a=b'
}
input_from_file() { #@test
  foo() { "$TOOL" <(echo $'declare -- a=b'); }
  run foo
  assert_success
  assert_output 'declare -- a=b'
}
simple_string() { #@test
  foo() { echo $' declare \t --  a=b  ' | "$TOOL"; }
  run foo
  assert_success
  assert_output 'declare -- a=b'
}
simple_string_quoted_unnecessary() { #@test
  foo() { echo $' declare \t --  a="b"  ' | "$TOOL"; }
  run foo
  assert_success
  assert_output 'declare -- a=b'
}
simple_string_comment() { #@test
  foo() { echo $' declare \t --  a="b" # comment ' | "$TOOL"; }
  run foo
  assert_success
  assert_output 'declare -- a=b'
}
simple_string_quoted_necessary() { #@test
  foo() { echo $' declare \t --  a="b x"  ' | "$TOOL"; }
  run foo
  assert_success
  assert_output 'declare -- a="b x"'
}
posix_quotes() { #@test
  foo() { echo $' declare \t -- a=$'"'"$'b\\tx'"'"'' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -- a="b\tx"'
}
array_empty() { #@test
  foo() { echo $'declare -a a=()' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -a a=()'
}
array_non_empty() { #@test
  foo() { echo $'declare -a a=( 1 2 3)' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -a a=(\n  1\n  2\n  3\n)'
}
array_complex_values() { #@test
  foo() { echo $'declare -a foo=( "b" "c d" )' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -a foo=(\n  b\n  "c d"\n)'
}
array_sparse_indexes_not_supported() { #@test
  foo() { echo $'declare -a foo=( [1]="b" [3]="c d" )' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -a foo=(\n  b\n  "c d"\n)'
}
dict_empty() { #@test
  foo() { echo $'declare -a d=()' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -a d=()'
}
dict_non_empty() { #@test
  foo() { echo $'declare -A d=([a]=1 [b]=2 [c]=3)' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -A d=(\n  [a]=1\n  [b]=2\n  [c]=3\n)'
}
dict_complex_values() { #@test
  foo() { echo $'declare -A foo=( [a]="b" [b]="c d" )' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -A foo=(\n  [a]=b\n  [b]="c d"\n)'
}
dict_empty_keys() { #@test
  foo() { echo $'set -e; declare -A foo=( []=b )' | "$TOOL"; }
  run foo
  assert_failure
  assert_line -p 'bad array subscript' # in stderr
}
dict_complex_keys_with_sorting() { #@test
  foo() { echo $'declare -A foo=( ["a"]="b" ["b c"]="c d" [" d e "]="e f" )' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -A foo=(\n  [" d e "]="e f"\n  [a]=b\n  ["b c"]="c d"\n)'
}
dict_keys_with_newlines() { #@test
  foo() { echo $'declare -A foo=(["b\nc"]="c d")' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -A foo=(\n  ["b\nc"]="c d"\n)'
}
variable_changes_allowed_no_declare() { #@test
  foo() { echo $'declare -- a=b; a=c' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -- a=c'
}
variable_changes_allowed_redeclare() { #@test
  foo() { echo $'declare -- a=b; declare -- a=d' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -- a=d'
}
variable_changes_last_wins() { #@test
  foo() { echo $'declare -- a=b; declare -- b=c; declare -- a=d; declare -- c=d; a=f' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -- b=c\ndeclare -- a=f\ndeclare -- c=d'
}
variable_changes_braces_ok() { #@test
  foo() { echo $'declare -- a=b; declare -- b=c; { declare -- a=d; }; declare -- c=d; a=f' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -- b=c\ndeclare -- a=f\ndeclare -- c=d'
}
stderr_passed_through() { #@test
  foo() { echo $'declare -- a=b; echo "hi" 1>&2; declare -- b=a' | "$TOOL"; }
  run --separate-stderr foo
  assert_success
  assert_output $'declare -- a=b\ndeclare -- b=a'
  [ $stderr = 'hi' ]
}
stdout_passed_through_to_stderr() { #@test
  foo() { echo $'declare -- a=b; echo "hi"; declare -- b=a' | "$TOOL"; }
  run --separate-stderr foo
  assert_success
  assert_output $'declare -- a=b\ndeclare -- b=a'
  [ $stderr = 'hi' ]
}
sorting_multiline_keys() { #@test
  foo() { echo "declare -A a=([\$'8\nb']='1 2' [\$'8\na']='1 2' [3]=abc [f]=def)" | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -A a=(\n  [3]=abc\n  ["8\na"]="1 2"\n  ["8\nb"]="1 2"\n  [f]=def\n)'
}
disallowed_vars_underscore() { #@test
  foo() { echo "declare -- _=0" | "$TOOL"; }
  run foo
  assert_failure
  assert_line -p $'None of these may be changed'
}
disallowed_vars_HOME() { #@test
  foo() { echo "declare -- HOME=0" | "$TOOL"; }
  run foo
  assert_failure
  assert_line -p $'None of these may be changed'
}
disallowed_vars_PATH() { #@test
  foo() { echo "declare -- PATH=0" | "$TOOL"; }
  run foo
  assert_failure
  assert_line -p $'None of these may be changed'
}
disallowed_vars_three_underscores() { #@test
  foo() { echo "declare -- ___x=0" | "$TOOL"; }
  run foo
  assert_failure
  assert_line -p $'names starting with ___ are reserved'
}
env_vars_allowed_HOME() { #@test
  foo() { echo $'declare -- x=$HOME' | env HOME=/my/home "$TOOL"; }
  run foo
  assert_success
  assert_output "declare -- x=\"/my/home\""
}
env_vars_allowed_USER() { #@test
  foo() { echo $'declare -- x=$USER' | env USER=joe "$TOOL"; }
  run foo
  assert_success
  assert_output "declare -- x=joe"
}
env_vars_disallowed() { #@test
  foo() { echo $'declare -- x=$J_RANDOM' | env J_RANDOM=joe "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -- x=""'
}
env_vars_disallowed_unset() { #@test
  foo() { echo $'set -u; declare -- x=$J_RANDOM' | env J_RANDOM=joe "$TOOL"; }
  run foo
  assert_failure
  assert_line -p $'J_RANDOM: unbound variable'
}
xtrace_allowed() { #@test
  foo() { echo $'declare -- a=b; set -x; uptime; set +x; declare -- b=a' | "$TOOL"; }
  run --separate-stderr foo
  assert_success
  assert_output $'declare -- a=b\ndeclare -- b=a'
  [[ $stderr =~ '+ uptime' ]]
}
script_failure_no_output() { #@test
  foo() { echo $'set -e; declare -- a=b; false; declare -- b=a' | "$TOOL"; }
  run --separate-stderr foo
  assert_failure
  assert_output $''
  [[ $stderr =~ 'STOPPING due to failure' ]]
}
script_early_exit_terminates_silently() { #@test
  foo() { echo $'declare -- a=b; exit; declare -- b=a' | "$TOOL"; }
  run --separate-stderr foo
  assert_success
  assert_output $''
  [[ $stderr == '' ]]
}
script_early_failure_terminates_with_message() { #@test
  foo() { echo $'declare -- a=b; exit 1; declare -- b=a' | "$TOOL"; }
  run --separate-stderr foo
  assert_failure
  assert_output $''
  [[ $stderr =~ 'STOPPING due to failure' ]]
}
comment_line() { #@test
  foo() { echo $' # comment ' | "$TOOL"; }
  run foo
  assert_success
  assert_output ''
}
comment_in_array() { #@test
  foo() { echo $' declare -a a=( \n # comment \n b # comment \n c);' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -a a=(\n  b\n  c\n)'
}
comment_in_dict() { #@test
  foo() { echo $' declare -A a=( \n # comment \n [b]=1 # comment \n [c]=1);' | "$TOOL"; }
  run foo
  assert_success
  assert_output $'declare -A a=(\n  [b]=1\n  [c]=1\n)'
}
json_empty() { #@test
  foo() { echo $'' | "$TOOL" --json; }
  run foo
  assert_success
  assert_output $'{\n}'
}
json_string() { #@test
  foo() { echo $'declare -- a=b' | "$TOOL" --json; }
  run foo
  assert_success
  assert_output $'{\n  "a": "b"\n}'
}
json_array() { #@test
  foo() { echo $'declare -a foo=(1 2 3)' | "$TOOL" --json; }
  run foo
  assert_success
  assert_output $'{\n  "foo": [\n    "1",\n    "2",\n    "3"\n  ]\n}'
}
json_dict() { #@test
  foo() { echo $'declare -A bar=([b]=1 [a]=2 [c]=3)' | "$TOOL" --json; }
  run foo
  assert_success
  assert_output $'{\n  "bar": {\n    "a": "2",\n    "b": "1",\n    "c": "3"\n  }\n}'
}
json_multi() { #@test
  foo() { echo $'declare -- a=b; declare -a foo=(1 2 3); declare -A bar=([b]=1 [a]=2 [c]=3)' | "$TOOL" --json; }
  run foo
  assert_success
  assert_output $'{\n  "a": "b",
  "foo": [\n    "1",\n    "2",\n    "3"\n  ],
  "bar": {\n    "a": "2",\n    "b": "1",\n    "c": "3"\n  }\n}'
}
json_special_chars() { #@test
  foo() { echo "declare -- a=$'A\\\\B\\\"C\\bD\\fE\\nF\\rG\\tH'" | "$TOOL" --json; }
  run foo
  assert_success
  assert_output $'{\n  "a": "A\\\\B\\"C\\bD\\fE\\nF\\rG\\tH"\n}'
}
