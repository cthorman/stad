#!/usr/bin/env bash

STAD="$(dirname "$BASH_SOURCE")"/../bin/stad
STAD_BASHEVAL="$(dirname "$BASH_SOURCE")"/../bin/stad-basheval

# We use bats-assert, bats-support, and bats-file libraries -- see:
# https://github.com/ztombol/bats-docs

source "$(dirname "$BASH_SOURCE")"/test_helper/*.bash
